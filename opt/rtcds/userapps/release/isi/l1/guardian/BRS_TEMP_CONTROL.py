from guardian import GuardState
import time
import cdsutils
import math

#nominal = 'HPI_SERVO_ON_CTIDAL'

class INIT(GuardState):
    request = True

class SERVO_ON(GuardState):
    index = 10
    request = True

    def main(self):
	self.set_point = 2400/100 #divide by 100 to get in degC
	self.timer['wait'] = 3600 # initialize timer for the run loop
    def run(self):
	gain = 0.0358 #fitting gain for voltage to brs temperature for ETMY
        offset = -0.2469 #fitting offset for voltage to brs temperature for ETMY
	if self.timer['wait']:
	    temp = cdsutils.avg(-10,'ISI-GND_BRS_ETMY_TEMPL',stddev=False)
	    diff = temp/100.0-self.set_point
            print ' ' + str(diff) + ' degC away from setpoint'
            if (diff-offset) >= 0: 
                volt_in = math.sqrt((diff-offset)/gain)
#               ezca['ISI-GND_BRS_ETMY_HEATCTRLIN'] = volt_in
                # Save the time to a file for offloading
                pathname = '/opt/rtcds/userapps/trunk/isi/l1/scripts/'
                filename = 'ey_brs_drive.txt'
                with open (pathname + filename, "a") as f:
                    f.write(str(volt_in) + ' ' + str(diff) + '\n')
            self.timer['wait']=3600
	    # Save the time to a file for offloading
#            pathname = '/opt/rtcds/userapps/trunk/isi/l1/scripts/'
#            filename = 'ex_brs_drive.txt'
#            with open (pathname + filename, "a") as f:
#                f.write(str(volt_in) + ' ' + str(diff) + '\n')
	return True

class HOLD_SERVO(GuardState):
    index = 20
    request = True

    def main(self):
	return
    def run(self):
        return True
##################################################################

edges = [
    ('INIT','SERVO_ON'),
    ('SERVO_ON', 'HOLD_SERVO'),
]


